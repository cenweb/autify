import sys

#Class imports
from core.core import Core
from core.fileProcessor import FileProcessor
from core.metadataProcessor import MetadataProcessor
from core.jsonProcessor import JsonProcessor

METADATA_FLAG = "--metadata"

#Dependency injection to core logic
core = Core(FileProcessor(), MetadataProcessor(), JsonProcessor())

if (sys.argv[1] == METADATA_FLAG):
    core.printMetadata(sys.argv[2])
else:
    core.iterateArgs(sys.argv[1:])