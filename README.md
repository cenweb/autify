# README #

This README would normally document whatever steps are necessary to get your application up and running.

### A.  How to run using Dockerfile ###
1. docker build --no-cache  -t autify .
2. docker run -dit --name autify autify
3. docker exec -it autify bash

### B. Once inside container, the following commands will work(EXAMPLE): ###
Section 1. python3 main.py https://google.com https://autify.com
Section 2. python3 main.py --metadata https://google.com


### C. How to run without using Dockerfile ###
Prerequisites: python3, pip3
1. pip3 install -r ./core/requirements.txt
2. Then use python3 command(above, part B) to execute sections 1 and 2 of the exercise

### Points for improvement ###
-> If there is extra time(as metioned in the exercise itself), it would have been better if images are archived locally so web pages will display properly offline
-> If there is extra time, it is also nice to include multithreading to make downloads of multiple web pages faster.