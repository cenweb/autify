FROM python:3

#Set work directory
WORKDIR /usr/src/autify

#Copy source to workdir
COPY . .

#Install needed packages using pip
RUN pip3 install -r ./core/requirements.txt

