import json

#Class for processing JSON strings
class JsonProcessor:

    #Method to convert dictionary to json string
    #Params:
    #objDict -> dictionary to be converted into JSON
    def getDictToJson(self, objDict):
        return json.dumps({obj: objDict[obj].__dict__ for obj in objDict})

    #Method for converting JSON object to dictionary
    #Params:
    #str -> JSON string to be converted into dictionary
    def getJsonToDict(self, str):
        return json.loads(str)
       