import os

#Class which will handle writing to a file
class FileProcessor:
    def __init__(self):
        self.__executePath = os.getcwd()

    #Writes string to a file
    #Params:
    #fileName -> fileName of file to be generated
    #textStr -> string to be written in the file
    def writeToFile(self, fileName, textStr):
        file = open(os.path.join(self.__executePath, fileName), "w")
        file.write(textStr)
        file.close()


    #Reads data from file
    #Params:
    #fileName -> fileName of file to be read
    def readFile(self, fileName):
        file = open(os.path.join(self.__executePath, fileName), "r")
        contents = file.read()
        return contents
        