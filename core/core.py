import requests

HTML_FILE_EXTENSION = ".html"
METADATA_FILE = "metadata.json"

#Handles the main business logic
class Core:
    #Constructor. Dependency injection is utilized so it is easier to mock/modify classes in the future
    #Params:
    #fileProcessor -> fileProcess object. The one in charge of reading/writing to files
    #metadataProcessor -> metadataProcessor object. The one in charge of constructing/deconstructing metadata from different web pages
    #jsonProcessor -> jsonProcessor object. The one in charge of encoding/decoding JSON  strings
    def __init__(self, fileProcessor, metadataProcessor, jsonProcessor):
        #Dependency Injection
        self.__fileProcessor = fileProcessor
        self.__metaDataProcessor = metadataProcessor
        self.__jsonProcessor = jsonProcessor
        
        #Dictionary which will hold data from: url<->metadata
        #If there are saved metadata from previous run, get it initially so previous
        #metadata entries will not be deleted incase of multiple runs
        self.__dataDict = self.__getSavedMetadata()

    #Method which will process each URL
    #1. send a get request to target URL
    #2. generate metadata of the response
    #3. write response to a file
    def __processUrl(self, url):
        #Gets response from URL
        responseText = requests.get(url).text

        #Gets all required metadata
        metadata = self.__metaDataProcessor.getMetadata(url, responseText)
        #Add current metadata to list of all metadata per url
        self.__dataDict[url] = metadata

        #generate filename based from URL
        fileName = url.split("/")[-1] + HTML_FILE_EXTENSION
        #Write to a file
        self.__fileProcessor.writeToFile(fileName, responseText)

    #Method which will save metadata of each URL
    #This method will convert dictionary to JSON so details can be retrieved later
    def __saveMetadata(self):
        metadataJson = self.__jsonProcessor.getDictToJson(self.__dataDict)
        self.__fileProcessor.writeToFile(METADATA_FILE, metadataJson)

    #Method to get saved metadata in the file
    #In case of errors, return empty dictionary
    def __getSavedMetadata(self):
        #Tries to read metadata from JSON
        try:
            metadataStr = self.__fileProcessor.readFile(METADATA_FILE)
            objDict = self.__jsonProcessor.getJsonToDict(metadataStr)
            metadataDict = self.__metaDataProcessor.decodeMetadata(objDict)
        except:
            metadataDict = {}

        return metadataDict

    #Process the list of URL arguments
    #Params:
    #args -> user supplied arguments
    def iterateArgs(self, args):
        for url in args:
            try:
                self.__processUrl(url)
            except Exception as e:
                #In case of errors, print error message
                print('Error for {0}: \n {1} \n'.format(url, e))

        self.__saveMetadata()

    #Core method for getting metadata of saved webpage
    #Params:
    #url -> will print the metadata of supplied URL
    def printMetadata(self, url):
        metadataDict = self.__getSavedMetadata()

        #Checks if metadata for a URL is available
        if (url in metadataDict):
            target = metadataDict[url]
            #Print metadata results
            print('site: {0}'.format(target.site))  
            print('num_links: {0}'.format(target.num_links))
            print('images: {0}'.format(target.images))   
            print('last_fetch: {0}'.format(target.last_fetch))   
        else:
            print("HTML for this URL is not yet generated")
        


