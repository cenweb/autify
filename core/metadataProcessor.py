import re
import datetime

#Class for processing metadata of a given URL
class MetadataProcessor:

    #Gets all count
    #Params:
    #url -> target url string
    #htmlText -> html text retrieved from the URL
    def getMetadata(self, url, htmlText):
        metadata = MetadataStruct()

        metadata.site = self.__getSiteName(url)
        metadata.images = self.__getImageCount(htmlText)
        metadata.num_links = self.__getLinkCount(htmlText)
        metadata.last_fetch = self.__getFormattedTimestamp()
        
        return metadata

    #Converts JSON dict to dict and MetadataStruct object combination
    def decodeMetadata(self, objDict):
        metaDict = {}

        for key in objDict:
            metadata = MetadataStruct()
            metadata.site = objDict[key]['site']
            metadata.images = objDict[key]['images']
            metadata.num_links = objDict[key]['num_links']
            metadata.last_fetch = objDict[key]['last_fetch']
            metaDict[key] = metadata

        return metaDict

    #Method that gets site name based from URL
    #Params:
    #url -> target URL
    def __getSiteName(self, url):
        return url.split("/")[-1]

    #Gets image count
    #Params:
    #htmlText -> html text retrieved from the URL
    def __getImageCount(self, htmlText):
        #count image tags
        imageList = re.findall('<img.*?>', htmlText)
        return len(imageList)

    #Gets link count
    #Params:
    #htmlText -> html text retrieved from the URL
    def __getLinkCount(self, htmlText):
        #count a href tags
        linkList = re.findall('<a href.*?>', htmlText)
        return len(linkList)

    #Gets formatted UTC timestamp
    def __getFormattedTimestamp(self):
        utcnow = datetime.datetime.utcnow()
        formattedTime = utcnow.strftime('%a %b %d %Y %H:%M UTC')
        return formattedTime


#Structure of metadata for each URL
class MetadataStruct:
    def __init__(self):
        self.site = "" #string
        self.num_links = 0 #integer 
        self.images = 0 #integer 
        self.last_fetch = "" #string



